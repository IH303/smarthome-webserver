var showingLogin;
setShowingLogin(true,null);
function setShowingLogin(showing, onLoaded) {
    if(showing) {
        showingLogin = true;
        $("body").load("login.html",function () {
            if(onLoaded)onLoaded();
        });
    }
    else{
        showingLogin = false;
        $("body").load("main.html",function () {
            if(onLoaded)onLoaded();
            checkAccess();
            refreshEmptyInfo(2000);
        });
    }

}

function checkAccess() {
    var access = $("#server-access>span");
    if(/(192[.]168[.]|localhost|[.]local)/.test(location.hostname)){
        access.html("lokal");
    }
    else{
        access.html("extern");
    }
}

function loginSubmit() {
    showLoginInfo("connecting");
    var user = document.forms["login"]["user"].value;
    var pass = document.forms["login"]["pass"].value;
    connect(user,pass);
}

function onLoginClick() {

    var user = $("#user-input").val();
    var pass = $("#pass-input").val();

}

function showLoginInfo(infoData) {
    var info = $("#connecting p")
    var loader = $(".lds-ripple");
    if(infoData==="connecting"){
        info.css("visibility", "visible");
        info.css("color", "black");
        loader.css("visibility", "visible");
        info.html("Verbindung wird hergestellt")
    }
    if(infoData==="hidden"){
        info.css("visibility", "hidden");
        loader.css("visibility", "hidden");
    }
    if(infoData==="fail"){
        info.css("visibility", "visible");
        info.css("color", "red");
        loader.css("visibility", "hidden");
        info.html("Anmeldung fehlgeschlagen")
    }
}
