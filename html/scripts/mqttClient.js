var msgLog;
var clientCount=1;
var connectionsField;
function logMsg(msg){
    if(msgLog===undefined) return;

    msgLog.val(msgLog.val()+ getTime()+" -" + msg+"\n");
    msgLog.scrollTop(msgLog[0].scrollHeight);
}

function getTime()
{
    var dt = (new Date());
    return (dt.getHours() < 10 ? '0' : '') + dt.getHours() + ":" +
        (dt.getMinutes() < 10 ? '0' : '') + dt.getMinutes() + ":" +
        (dt.getSeconds() < 10 ? '0' : '') + dt.getSeconds() + ":" +
        (dt.getMilliseconds() < 100 ? (dt.getMilliseconds()<10 ? '00' : '0') : '') + dt.getMilliseconds();
}

var client;

function connect(username,password){
    client = new Paho.MQTT.Client(location.hostname, 4444, username);
    //client = new Paho.MQTT.Client("smarthome-dashboard.my-router.de", 4444, username);
    client.onConnectionLost = onConnectionLost;
    client.onMessageArrived = onMessageArrived;
    client.connect({userName:username, password:password,onSuccess:onConnect,onFailure:onFailedConnect});
}



// called when the client connects
function onConnect() {
    if(showingLogin){
        setShowingLogin(false,function () {
            $("#user-id>span").html(client.clientId);
            db=$("#dashboard");
            msgLog = $("#message-log textarea");
            msgLog.val("");
            connectionsField = $("#connected-users .bordered-text")[0];
        });
    }
    // Once a connection has been made, make a subscription and send a message.
    console.log("connected");
    client.subscribe("$SYS/broker/log/#");
    client.subscribe("connect/#",{qos:1});
    client.subscribe("disconnect/+");
    client.subscribe("update/#");
    client.subscribe("$SYS/broker/clients/connected")
    client.send("discover","");
}

function onFailedConnect() {
    if(showingLogin){
        showLoginInfo("fail");
    }
}

// called when the client loses its connection
function onConnectionLost(responseObject) {
    if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost:"+responseObject.errorMessage);
    }

}

function sendTest(topic, message) {
    client.send(topic,message);
}

// called when a message arrives
function onMessageArrived(message) {
    console.log("onMessageArrived: \ntopic: "+message.destinationName+"\nmessage: "+message.payloadString);
    if(message.destinationName.startsWith("connect")){
        var tpc = message.destinationName.split("/");
        var msg = message.payloadString.split(" ");
        addModule(tpc[1],tpc[2],function () {
            if(tpc[1]==="led"){
                led_updatePower(tpc[2],msg[0]);
                led_updateColor(tpc[2],msg[1]);
                led_updateNewColor(tpc[2],msg[1]);
            }
            else if(tpc[1]==="temp"){
                temp_updatePower(tpc[2],msg[0]);
                temp_updateTemperature(tpc[2],msg[1]);
                temp_updateHumidity(tpc[2],msg[2]);
            }
            else if(tpc[1]==="fan"){
                fan_updatePower(tpc[2],msg[0]);
                fan_updateMode(tpc[2],msg[1]);
                fan_updateSpeed(tpc[2],msg[2]);
                fan_updateReference(tpc[2],msg[3]);
                fan_updateTarget(tpc[2],msg[4]);
            }
        });
    }
    else if(message.destinationName.startsWith("disconnect")){
        var arr = message.destinationName.split("/");
        removeModule(arr[1]);
        setTimeout(function () {
            client.send("discover","");
        },300);
    }
    else if(message.destinationName.startsWith("update")){
        var tpc = message.destinationName.split("/");
        if(tpc[1]==="led") led_update(tpc,message.payloadString);
        else if(tpc[1]==="temp") temp_update(tpc,message.payloadString);
        else if(tpc[1]==="fan") fan_update(tpc,message.payloadString);
    }
    else if(message.destinationName.startsWith("$SYS/broker/log/")){
        //console.log("ws");
        var str = message.payloadString.substring(message.payloadString.indexOf(":") + 1);
        logMsg(str);
    }
    else if(message.destinationName.startsWith("$SYS/broker/clients/connected")){
        clientCount=message.payloadString;
        setTimeout(function () {
            calcUsers();
        },2000);
    }
}

function calcUsers() {
    let newUsers = Math.max(clientCount-moduleCount,1);
    connectionsField.innerHTML = newUsers;
}
