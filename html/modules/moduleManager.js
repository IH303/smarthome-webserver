var db;
var moduleCount = 0;
var adding = "";



function Module(type,id,successHandler) {
	var newModule = this;
	this.type = type;
	this.id = id;
	this.htmlObj = $('<div>').load("modules/"+type+".html", function () {
		moduleCount++;
		refreshEmptyInfo();
		newModule.header = newModule.htmlObj.find(".module-header");
		newModule.mm = newModule.htmlObj.find(".module-main");
		newModule.header.click(setCollapse(newModule.mm));
		newModule.header.find(".module-id").text(id);
		newModule.cp = newModule.htmlObj.find(".control-module");
		newModule.htmlObj.attr("id",newModule.id);
		setTimeout(
			function()
			{
				db.animate({height: calcDbHeight()}, 200);
			}, 200);
		setTimeout(
			function()
			{
				newModule.cp.addClass("shown");
                if(type==="led"){
					newModule.htmlObj.find(".jscolor").get(0).jscolor = new jscolor(newModule.htmlObj.find(".jscolor").get(0));
					newModule.htmlObj.find(".jscolor").get(1).jscolor = new jscolor(newModule.htmlObj.find(".jscolor").get(1));
                }
				if(successHandler!==undefined)successHandler();

			}, 400);
	});
}




function calcDbHeight() {
	let h = 10;
	$(".control-module").each(function(){
		h += $(this).outerHeight();
		h += 20;
	});
	return h;
}

function refreshEmptyInfo(delay) {
	setTimeout(function () {
		let info = $("#no-module-info");
		moduleCount > 0? info.fadeOut() : info.fadeIn();
	},delay);

}


function addModule(type,id, successHandler) {
	if(adding===id)return;
	adding = id;
	let cp = $("#" + id);
	if(cp.length > 0){
		adding = "";
		return;
	}
	let newModule = new Module(type, id, function(){
		successHandler();
		adding="";
	});
	$("#dashboard").append(newModule.htmlObj);
}

function removeModule(id) {
	var cp = $("#"+id).find(".control-module");
	if(cp.length === 0) return;
	//var index = $(".control-module").index(cp);
	cp.removeClass("shown");
	cp.animate({height: 0}, 200,function () {
		$("#"+id).remove();
		setTimeout(
			function()
			{
				db.animate({height: calcDbHeight()}, 200);
			}, 200);
	});
	moduleCount--;
	refreshEmptyInfo(1000);
}


