//Eventhandler, der Modul-Anzeigen ein- bzw. ausklappt
var timeoutCollapse;
function setCollapse(mm){
	return function () {
		"use strict";
		var mobile = $(window).width() <= 630;
		var offset = mobile ? 0 : 15;
		clearTimeout(timeoutCollapse);
		if (mm.css("opacity") === "1") {
			mm.animate({opacity: 0.1}, 200, function () {
				mm.animate({height: 15 - offset}, 200,function () {
					db.animate({height: calcDbHeight()}, 200);
				});
			});
		} else {
			db.animate({height: calcDbHeight()+mm.get(0).scrollHeight - offset}, 200);
			timeoutCollapse = setTimeout(function () {
				mm.animate({height: mm.get(0).scrollHeight - offset}, 200, function () {
					mm.animate({opacity: 1}, 200,function () {
						db.height(calcDbHeight());
					});
				});
			},400);

		}

	}
}

//Eventhandler (bei Änderung von Displaybreite), der alle Modul-Anzeigen wieder ausklappt
var oldWidth = $(window).width();
$(window).on('resize', function(){
	"use strict";
	if ($(window).width()!==oldWidth){
		oldWidth = $(window).width();
		$(".module-main").each(function(){
			$(this).css("opacity",1);
			$(this).css("height", "100%");
		});
		if(db!=null)db.height(calcDbHeight());
	}
});