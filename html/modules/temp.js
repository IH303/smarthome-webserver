let tempFakeClick = false;

/*function led_setColor(btn) {
    var id = $(btn).closest(".control-module").find(".module-id").text();
    var color = $(btn).closest(".module-main").find(".new-color")[0].value;
    client.send("set/led/"+id+"/color",color);
}*/

/*function led_updateColor(id, color) {
    var ac = $("#"+id).find(".act-color");
    if(ac.length === 0) return;
    color = "00000" + color;
    color = color.substr(-6);
    ac[0].jscolor.fromString(color);
}*/

/*function led_updateNewColor(id, color) {
    var nc = $("#"+id).find(".new-color");
    if(nc.length === 0) return;
    color = "00000" + color;
    color = color.substr(-6);
    nc[0].jscolor.fromString(color);
}*/


function temp_setPower(btn) {
    if(tempFakeClick){
        tempFakeClick=false;
        return;
    }
    let id = $(btn).closest(".control-module").find(".module-id").text();
    let checked = btn.checked;
    client.send("set/temp/"+id+"/power",checked?"on":"off");
}


function temp_updatePower(id,power) {
    let sw = $("#" + id).find(".switch>input")[0];
    if ((power==="on") !== sw.checked) {
        tempFakeClick=true;
        sw.click();
    }
}

function temp_updateTemperature(id,temperature) {
    let t = $("#" + id).find(".temperature")[0];
    if(t === undefined) return;
    t.innerHTML=temperature.replace(".",",");
}

function temp_updateHumidity(id,humidity) {
    let h = $("#" + id).find(".humidity")[0];
    if(h === undefined) return;
    h.innerHTML=humidity;
}

function temp_update(topic, message) {
    if(topic[3]==="power"){
        temp_updatePower(topic[2],message);
    }
    else if(topic[3]==="temperature"){
        temp_updateTemperature(topic[2],message);
    }
    else if(topic[3]==="humidity"){
        temp_updateHumidity(topic[2],message);
    }
}