var fanFakeClick=false;

function fan_auto(btn) {
    let id = $(btn).closest(".control-module").find(".module-id").text();
    //$(btn).toggleClass("down");
    client.send("set/fan/"+id+"/mode",$(btn).hasClass("down")?"manual":"auto");
}

function fan_setMode(id, auto) {
    let spans = $("#"+id+" .module-main>span");
    let line = $("#"+id+" .module-main>hr")[0];
    let btn = $("#"+id+" button")
    if(auto){
        spans.eq(0).hide();
        spans.eq(1).show();
        line.style.visibility="";
        spans.eq(2).css("pointer-events","auto");
        spans.eq(2)[0].style.visibility="visible";
        btn.addClass("down");
    }
    else{
        spans.eq(1).hide();
        line.style.visibility="hidden";
        spans.eq(0).show();
        spans.eq(2).css("pointer-events","none");
        spans.eq(2)[0].style.visibility="hidden";
        btn.removeClass("down");
    }
}

function fan_setPower(btn) {
    if(fanFakeClick){
        fanFakeClick=false;
        return;
    }
    var id = $(btn).closest(".control-module").find(".module-id").text();
    var checked = btn.checked;
    client.send("set/fan/"+id+"/power",checked?"on":"off");
}

function fan_updatePower(id,power) {
    var sw = $("#"+id).find(".switch>input")[0];
    if ((power==="on") !== sw.checked) {
        fanFakeClick=true;
        sw.click();
    }
}

function fan_updateMode(id, mode) {
    fan_setMode(id,mode==="auto");
}

function fan_setSpeed(btn) {
    let id = $(btn).closest(".control-module").find(".module-id").text();
    let field = $("#"+id+" .module-main>span>input").eq(0);
    field[0].value=Math.min(100,Math.max(0,field[0].value))
    client.send("set/fan/"+id+"/speed",""+field[0].value);
    field.prop('readonly', true);
    setTimeout(function () {
        field.prop('readonly', false);
    },200)
}

function fan_updateSpeed(id,speed) {
    $("#"+id+" .module-main>span>input").eq(0)[0].value=speed;
}

function fan_setReference(btn) {
    let id = $(btn).closest(".control-module").find(".module-id").text();
    let field = $("#"+id+" .module-main>span>input").eq(2);
    field[0].value=Math.min(255,Math.max(0,field[0].value))
    client.send("set/fan/"+id+"/ref",""+field[0].value);
    field.prop('readonly', true);
    setTimeout(function () {
        field.prop('readonly', false);
    },200)
}

function fan_updateReference(id,ref) {
    $("#"+id+" .module-main>span>input").eq(2)[0].value=ref;
}

function fan_setTarget(btn) {
    let id = $(btn).closest(".control-module").find(".module-id").text();
    let field = $("#"+id+" .module-main>span>input").eq(1);
    field[0].value=Math.min(40,Math.max(0,field[0].value))
    client.send("set/fan/"+id+"/target",""+field[0].value);
    field.prop('readonly', true);
    setTimeout(function () {
        field.prop('readonly', false);
    },200)
}

function fan_updateTarget(id,pTemp) {
    $("#"+id+" .module-main>span>input").eq(1)[0].value=pTemp;
}

function fan_update(topic, message) {
    if(topic[3]==="power"){
        fan_updatePower(topic[2],message);
    }
    else if(topic[3]==="mode"){
        fan_updateMode(topic[2],message);
    }
    else if(topic[3]==="speed"){
        fan_updateSpeed(topic[2],message);
    }
    else if(topic[3]==="ref"){
        fan_updateReference(topic[2],message);
    }
    else if(topic[3]==="target"){
        fan_updateTarget(topic[2],message);
    }
}