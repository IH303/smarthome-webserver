var ledFakeClick=false;

function led_setColor(btn) {
    var id = $(btn).closest(".control-module").find(".module-id").text();
    var color = $(btn).closest(".module-main").find(".new-color")[0].value;
    client.send("set/led/"+id+"/color",color);
}

function led_updateColor(id, color) {
    var ac = $("#"+id).find(".act-color");
    if(ac.length === 0) return;
    color = "00000" + color;
    color = color.substr(-6);
    ac[0].jscolor.fromString(color);
}

function led_updateNewColor(id, color) {
    var nc = $("#"+id).find(".new-color");
    if(nc.length === 0) return;
    color = "00000" + color;
    color = color.substr(-6);
    nc[0].jscolor.fromString(color);
}

function led_setPower(btn) {
    if(ledFakeClick){
        ledFakeClick=false;
        return;
    }
    var id = $(btn).closest(".control-module").find(".module-id").text();
    var checked = btn.checked;
    client.send("set/led/"+id+"/power",checked?"on":"off");
}

function led_updatePower(id,power) {
    var sw = $("#"+id).find(".switch>input")[0];
    if ((power==="on") !== sw.checked) {
        ledFakeClick=true;
        sw.click();
    }
}

function led_update(topic, message) {
    if(topic[3]==="power"){
        led_updatePower(topic[2],message);
    }
    else if(topic[3]==="color"){
        led_updateColor(topic[2],message);
    }
}